////////////// GEO ///////////////////////
function onGeoSuccess(position) {
    var lat = position.coords.latitude;
    var longi = position.coords.longitude;
    codeLatLng(lat,longi);
}

function onGeoError(error){
    console.log("Getting the error"+error.code + "\nerror mesg :" +error.message);
}

function codeLatLng(lat, lng) {
    var geocoder = new google.maps.Geocoder();
    var latlng = new google.maps.LatLng(lat, lng);
    geocoder.geocode({'latLng': latlng}, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            if (results[1]) {
                for (var i=0; i<results[0].address_components.length; i++) {
                    for (var b=0;b<results[0].address_components[i].types.length;b++) {
                        if (results[0].address_components[i].types[b] == "sublocality") {
                            suburb = results[0].address_components[i];
                            break;
                        }
                        if (results[0].address_components[i].types[b] == "administrative_area_level_1") {
                            city = results[0].address_components[i];
                            break;
                        }
                    }
                }
                localStorage.setItem('Location', suburb.short_name + ", " + city.long_name);
            }else{
                return false;
            }
        }else{
            console.log("Geocoder failed due to: " + status);
        }
    });
}

function OpenInMapsLinks(){
    if(myApp.device.os == 'ios'){
        var OpenInMapsurl = "maps://maps.apple.com/?daddr=16 Langford Road,Westville,3629&dirflg=d";
    }else if(myApp.device.os == 'android'){
        var OpenInMapsurl = "https://www.google.co.za/maps/place/16+Langford+Rd,Westville,3629";
    }
    return OpenInMapsurl;
}

function OpenInMapsLinksPopulate(){
    if(myApp.device.os == 'ios'){
        $$('.open-in-maps-link-populate').attr('href','http://maps.apple.com/?daddr=16 Langford Road,Westville,3629&dirflg=d');
    }else if(myApp.device.os == 'android'){
        $$('.open-in-maps-link-populate').attr('href','https://www.google.co.za/maps/place/16+Langford+Rd,Westville,3629');
    }
}
////////////// GEO ///////////////////////