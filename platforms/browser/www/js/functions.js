////////////// Tournaments ///////////////////////
function AddTournament(){
    var storedData = myApp.formGetData('UserDetails');
    $('#AddTournament .btn-submit').on('click',function(){
        var formname = 'AddTournament';
        if(validation(formname)){   
            preloader('', 'body');
            $.ajax({
                type: "POST",
                dataType: 'jsonp',
                url : "http://groot.dedicated.co.za/cric-it/api",
                data : 'wrapper=tournament&type=AddTournament&' + FindFields(formname) + '&UserID=' + storedData.User.id + '&_token=t8Y]^RhRe,xp@Xfk76%$',
                success : function(data){
                    if(data.status == 'true'){
                        mainView.router.loadPage({
                            url: 'view-tournament.html',
                            query: {
                                TournamentID: data.result
                            }
                        });
                    }else{
                        mainView.router.loadPage({
                            url: 'add-tournament.html'
                        });
                    }
                    preloaderRemove();
                    return false;
                },
                error : function(data){
                    preloaderRemove();
                    myApp.addNotification({
                        title: 'Error',
                        message: 'Oops! Something went wrong, please try again later.',
                        hold: 5000
                    });
                }
            },"json");
            return false;
        }    
        return false;
    });
}

function GetTournaments(){
    preloader('', 'body');
    $.ajax({
        type: "POST",
        dataType: 'jsonp',
        url : "http://groot.dedicated.co.za/cric-it/api",
        data : 'wrapper=tournament&type=GetTournaments&_token=t8Y]^RhRe,xp@Xfk76%$',
        success : function(data){
            console.log(data);
            var Html = '';
            $.each(data,function(a,b){
                Html += '<div class="content-block-title">' + a + ' Tournaments</div>';
                Html += '<div class="list-block">';
                    Html += '<div class="list-group">';
                        Html += '<ul>';
                            $.each(b,function(c,d){
                                Html += '<li class="item-content">';
                                    Html += '<a href = "#" data-rel="' + d.Details.id + '" class = "item-link view-tournament">';
                                        Html += '<div class="item-inner">';
                                            Html += '<div class="item-title"><img src="'+d.Details.Logo+'"/><div class="teamnames">'+d.Details.Name+'</div></div>';
                                        Html += '</div>';
                                    Html += '</a>';
                                Html += '</li>';
                            });
                            Html += '</li>';
                        Html += '</ul>';
                    Html += '</div>';
                Html += '</div>';
            });
            
            $('[data-page="view-tournaments"].page h1').after(Html);
            preloaderRemove();
            $('.view-tournament').on('click',function(){
                mainView.router.loadPage({
                    url: 'view-tournament.html',
                    query: {
                        TournamentID: $(this).attr('data-rel')
                    }
                });
            });
        },
        error : function(data){
            preloaderRemove();
            myApp.addNotification({
                title: 'Error',
                message: 'Oops. Something went wrong!',
                hold: 5000
            });
        }
    },"json");
}

function ViewTournament(TournamentID = false, Refresh = false){
    $TournamentID = TournamentID;
    // preloader('', 'body');
    if(Refresh){
        $('.team-detail').html('');
    }
    var storedData = myApp.formGetData('UserDetails');
    $.ajax({
        type: "POST",
        dataType: 'jsonp',
        url : "http://groot.dedicated.co.za/cric-it/api",
        data : 'wrapper=tournament&type=ViewTournament&TournamentID=' + TournamentID + '&_token=t8Y]^RhRe,xp@Xfk76%$',
        success : function(data){
            console.log(data);
            $('[data-page="view-tournament"].page #team-details h1').html(data.Tournament.Name);
            $('[data-page="view-tournament"].page #team-details .format').html(data.Tournament.Format);
            $('[data-page="view-tournament"].page #team-details .format').addClass(data.Tournament.Format);
            $('[data-page="view-tournament"].page #team-details .logo img').attr('src',data.Tournament.Logo);
            var Html = '';
            var Teams = '';
            Html += '<div class="content-block-title">Teams: ';
            var SwipeOut = '';
            var SwipeOutContent = '';
            if(storedData.User.Role == 'administrator'){
                Html += '<i class="fa fa-plus fa-fw add-team-to-tournament" data-rel="' + data.Tournament.id + '" data-tournamentname="' + data.Tournament.Name + '" aria-hidden="true"></i>';
                SwipeOut = 'swipeout';
                SwipeOutContent = 'swipeout-content';
            }
            Html += '</div>';
            if(data.Teams.length > 0){
                Html += '<div class="list-block medialist">';
                    Html += '<ul>';
                        $.each(data.Teams,function(k,v){
                            Html += '<li class="' + SwipeOut + '" data-rel="' + v.id + '">';
                                Html += '<div class = "' + SwipeOutContent + ' item-content">';
                                    Html += '<div class = "item-inner">';
                                        Html += '<div class = "item-media">';
                                            Html += '<img src="' + v.ProfileIMG + '"/>';
                                        Html += '</div>';
                                        Html += '<div class = "item-title">';
                                            Html += v.FirstName + ' ' + v.LastName;
                                        Html += '</div>';
                                    Html += '</div>';
                                Html += '</div>';
                                if(storedData.User.Role == 'administrator'){
                                    Html += '<div class="swipeout-actions-right">';
                                        Html += '<a href="#" class="swipeout-delete" id="' + v.id + '" data-confirm="Are you sure want to delete ' + v.FirstName + '?" data-confirm-title="Delete?">Delete</a>';
                                    Html += '</div>';
                                }
                            Html += '</li>';
                        });
                    Html += '</ul>';
                Html += '</div>';
            }else{
                Html += '<p><em>No teams available</em></p>';
            }
            $('#team-details .team-detail ').html(Html);
            preloaderRemove();
            $$('.swipeout').on('deleted', function (e) {
                DeleteTeamFromTournament($(this).attr('data-rel'), TournamentID);
            });
            FindFormats('tournament');
        },
        error : function(data){
            preloaderRemove();
            myApp.addNotification({
                title: 'Error',
                message: 'Oops. Something went wrong!',
                hold: 5000
            });
        }
    },"json");
}

function AddTeamToTournament(TournamentID = false, TeamID = false){
    $.ajax({
        type: "POST",
        dataType: 'jsonp',
        url : "http://groot.dedicated.co.za/cric-it/api",
        data : 'wrapper=tournament&type=AddTeam&TournamentID=' + TournamentID + '&TeamID=' + TeamID + '&_token=t8Y]^RhRe,xp@Xfk76%$',
        success : function(data){
            preloaderRemove();
            myApp.addNotification({
                title: 'Tournament Update',
                message: data.result,
                hold: 5000
            });
            myApp.closeModal();
            mainView.router.loadPage({
                url: 'view-tournament.html',
                query: {
                    TournamentID: TournamentID
                }
            });
        },
        error : function(data){
            
        }
    },"json");
}

function onTournamentImageSuccess(imageURI) {
    preloader('', 'body');
    var options = new FileUploadOptions();
    options.fileKey="file";
    options.fileName=imageURI.substr(imageURI.lastIndexOf('/')+1);
    options.mimeType="image/jpeg";
    options.httpMethod = 'POST';
    var params = new Object();
    params.TournamentID = $TournamentID;
    params.wrapper = 'tournament';
    params.type = 'TournamentImageUpdate';

    options.params = params;
    options.chunkedMode = false;
    options.headers = {
        Connection: "close"
    }
    var ft = new FileTransfer();
    ft.upload(imageURI, "http://groot.dedicated.co.za/cric-it/api/index.php", UploadWin, UploadFail, options);
    var image = document.getElementById('tournament-img');
    image.src = imageURI;
    preloaderRemove();
    return false;
}

function onTournamentImageFail(message) {
    console.log('Failed because: ' + message);
}

function DeleteTeamFromTournament(UserID = false, TournamentID = false){
    $.ajax({
        type: "POST",
        dataType: 'jsonp',
        url : "http://groot.dedicated.co.za/cric-it/api",
        data : 'wrapper=teams&type=UserDeleteFromTeam&UserID=' + UserID + '&TeamID=' + TeamID + '&_token=t8Y]^RhRe,xp@Xfk76%$',
        success : function(data){
            preloaderRemove();
        },
        error : function(data){
            
        }
    },"json");
}
////////////// Tournaments ///////////////////////

////////////// Matches ///////////////////////
function AddMatch(){
    var storedData = myApp.formGetData('UserDetails');
    $('#AddMatch .btn-submit').on('click',function(){
        var formname = 'AddMatch';
        if(validation(formname)){   
            preloader('', 'body');
            $.ajax({
                type: "POST",
                dataType: 'jsonp',
                url : "http://groot.dedicated.co.za/cric-it/api",
                data : 'wrapper=match&type=AddMatch&' + FindFields(formname) + '&UserID=' + storedData.User.id + '&_token=t8Y]^RhRe,xp@Xfk76%$',
                success : function(data){
                    if(data.status == 'true'){
                        mainView.router.loadPage({
                            url: 'view-match.html',
                            query: {
                                MatchID: data.result
                            }
                        });
                    }else{
                         mainView.router.loadPage({
                            url: 'add-match.html'
                        });
                    }
                    preloaderRemove();
                    return false;
                },
                error : function(data){
                    preloaderRemove();
                    myApp.addNotification({
                        title: 'Error',
                        message: 'Oops! Something went wrong, please try again later.',
                        hold: 5000
                    });
                }
            },"json");
            return false;
        }    
        return false;
    });
}

function GetMatch(MatchID = false){
    preloader('', 'body');
    $.ajax({
        type: "POST",
        dataType: 'jsonp',
        url : "http://groot.dedicated.co.za/cric-it/api",
        data : 'wrapper=match&type=GetMatch&MatchID=' + MatchID + '&_token=t8Y]^RhRe,xp@Xfk76%$',
        success : function(data){
            console.log(data.result);
            var Html = '';
            var BattingTeam = data.result.BattingTeam;
            var BowlingTeam = data.result.BowlingTeam;
            var Settings = data.result.Settings;
            if(data.result.Settings.Team1Order == '0' && data.result.Settings.Team2Order == '0'){
                Html += '<h1>' + data.result.Settings.Team1Name + ' vs ' + data.result.Settings.Team2Name + '</h1>';
                Html += '<p class="date">' + data.result.CreatedDate + '</p>';
                Html += '<p>Choose which team is Batting first</p>';
                Html += '<div class="scorecard-detail" data-matchid="' + data.result.Settings.id + '" data-teamid="' + data.result.Settings.Team1ID + '">';
                    Html += '<h1>' + data.result.Settings.Team1Name + '</h1>';
                Html += '</div>';
                Html += '<div class="scorecard-detail" data-matchid="' + data.result.Settings.id + '" data-teamid="' + data.result.Settings.Team2ID + '">';
                    Html += '<h1>' + data.result.Settings.Team2Name + '</h1>';
                Html += '</div>';
            }else{
                Html += '<input id="current-over" class="hidden" value="' + BowlingTeam.CurrentOver + '"/>';
                Html += '<input id="current-over-id" class="hidden" value="' + BowlingTeam.CurrentOverID + '"/>';
                $('[data-page="view-match"] .standard-page').addClass('active-game');
                Html += '<div class="scorecard-detail">';
                    Html += '<div class="team">';
                        Html += '<h3>' + BattingTeam.Details.Name + '</h3>';
                        Html += '<div class="scores">';
                            Html += '<div class="score">' + BattingTeam.Score + ' - ' + BattingTeam.Wickets + '</div>';
                            Html += '<div class="overs">' + BattingTeam.Overs + ' Overs</div>';
                        Html += '</div>';
                    Html += '</div>';
                    Html += '<div class="vs">VS</div>';
                    Html += '<div class="team">';
                        Html += '<h3>' + BowlingTeam.Details.Name + '</h3>';
                        Html += '<div class="scores">';
                            Html += '<div class="score">' + BowlingTeam.Score + ' - ' + BowlingTeam.Wickets + '</div>';
                            Html += '<div class="overs">' + BowlingTeam.Overs + ' Overs</div>';
                        Html += '</div>';
                    Html += '</div>';
                    Html += '<div class="result">' + data.result.Result + '</div>';
                    Html += '<a class="refresh-game-data" data-matchid="' + data.result.Settings.id + '"><i class="fa fa-refresh" aria-hidden="true"></i></a>';
                Html += '</div>';

                /* Bowlers */

                HtmlBowlers = '<div class="bowlers">';
                    HtmlBowlers += '<input id="bowling-teamid" class="hidden" value="' + BowlingTeam.Details.id + '"/>';
                    HtmlBowlers += '<h3>' + BowlingTeam.Details.Name + ' <div class="pull-right">Bowlers</div></h3>';
                    var HideActionsBowler = true;
                    if(BowlingTeam.CurrentBowlers){
                        HtmlBowlers += '<table>';
                            HtmlBowlers += '<thead>';
                              HtmlBowlers += '<tr>';
                                HtmlBowlers += '<th class="label-cell">Bowlers</th>';
                                HtmlBowlers += '<th class="numeric-cell">O</th>';
                                HtmlBowlers += '<th class="numeric-cell">M</th>';
                                HtmlBowlers += '<th class="numeric-cell">R</th>';
                                HtmlBowlers += '<th class="numeric-cell">W</th>';
                                HtmlBowlers += '<th class="numeric-cell">NB</th>';
                                HtmlBowlers += '<th class="numeric-cell">WI</th>';
                                HtmlBowlers += '<th class="numeric-cell">OR</th>';
                              HtmlBowlers += '</tr>';
                            HtmlBowlers += '</thead>';
                            HtmlBowlers += '<tbody>';
                            $.each(BowlingTeam.CurrentBowlers,function(k,v){
                                HtmlBowlers += '<input id="bowler-' + k + '" class="current-bowlers hidden" value="' + v.Profile.User.id + '"/>';
                                HtmlBowlers += '<tr class="' + v.Status + '" data-playerid="' + v.Profile.User.id + '" data-matchid="' + MatchID + '">';
                                    HtmlBowlers += '<td class="label-cell">' + v.Profile.User.FirstName + '</td>';
                                    HtmlBowlers += '<td class="numeric-cell">' + v.Score.Bowling.Overs + '</td>';
                                    HtmlBowlers += '<td class="numeric-cell">' + v.Score.Bowling.Maidens + '</td>';
                                    HtmlBowlers += '<td class="numeric-cell">' + v.Score.Bowling.Runs + '</td>';
                                    HtmlBowlers += '<td class="numeric-cell">' + v.Score.Bowling.Wides + '</td>';
                                    HtmlBowlers += '<td class="numeric-cell">' + v.Score.Bowling.NoBalls + '</td>';
                                    HtmlBowlers += '<td class="numeric-cell">' + v.Score.Bowling.Wickets + '</td>';
                                    HtmlBowlers += '<td class="numeric-cell">' + v.Score.Bowling.OverRate + '</td>';
                                HtmlBowlers += '</tr>';
                                if(v.Status == "active-bowler"){
                                    HideActionsBowler = false;
                                    HtmlBowlers += '<input id="active-bowler" class="hidden" value="' + v.Profile.User.id + '"/>';
                                    HtmlBowlers += '<tr class="' + v.Status + '" data-playerid="' + v.Profile.User.id + '" data-matchid="' + MatchID + '">';
                                        if(Settings.Status != 'Complete'){
                                            HtmlBowlers += '<td colspan="8"><button class="complete-over btn-primary" data-overid="' + v.CurrentOverID + '">Complete Over</button></td>';
                                        }
                                    HtmlBowlers += '</tr>';
                                }
                            });
                            HtmlBowlers += '</tbody>';
                        HtmlBowlers += '</table>';
                    }else{
                        if(Settings.Status != 'Complete'){
                            HtmlBowlers += '<div class="no-bowlers" data-matchid="' + MatchID + '" data-teamid="' + BowlingTeam.Details.id + '">';
                                HtmlBowlers += '<h3>Click here to add a Bowler</p>';
                            HtmlBowlers += '</div>';
                        }
                    }
                HtmlBowlers += '</div>';
                /* Bowlers */

                /* Batsmen */
                Html += '<div class="batsmen">';
                    Html += '<input id="batting-teamid" class="hidden" value="' + BattingTeam.Details.id + '"/>';
                    Html += '<h3>' + BattingTeam.Details.Name + ' <div class="pull-right">Batsmen</div></h3>';
                    var Count = 1;
                    var ActiveCount = 0;
                    var HideActions = true;
                    $.each(BattingTeam.CurrentBatsmen.Players,function(k,v){
                        if(v){
                            if(v.Status == "active-batsmen"){
                                ActiveCount++;
                                Html += '<input id="active-batsmen" class="hidden" value="'+v.Profile.User.id+'"/>';
                            }else{
                                Html += '<input id="partner-batsmen" class="hidden" value="'+v.Profile.User.id+'"/>';
                            }
                            Html += '<input id="batsmen-' + Count + '" class="hidden" value="'+v.Profile.User.id+'"/>';
                            HideActions = false;
                            Html += '<div class="batsman-' + Count + ' ' + v.Status + '" data-rel="' + Count + '" data-playerid="' + v.Profile.User.id + '" data-matchid="' + MatchID + '">';
                                Html += '<img src="img/empty-profile.jpg"/>';
                                Html += '<p>' + v.Profile.User.FirstName + ' ' + v.Profile.User.LastName + '</p>';
                                Html += '<div class="scores">';
                                    Html += '<span>' + v.Score.Batting.Runs + '</span> (' + v.Score.Batting.Balls + ')';
                                Html += '</div>';
                            Html += '</div>';
                            
                        }else{
                            if(Settings.Status != 'Complete'){
                                Html += '<input id="batsmen-' + Count + '" class="hidden" value="0"/>';
                                Html += '<div id="batsman-' + Count + '" class="batsman-' + Count + ' no-batsmen" data-rel="' + Count + '" data-matchid="' + MatchID + '" data-teamid="' + BattingTeam.Details.id + '">';
                                    Html += '<h3>Click here<br/>to add a<br/>Batsman</p>';
                                Html += '</div>';
                            }
                        }
                        if(Count == 1){
                            Html += '<div class="partnership">';
                                if(BattingTeam.CurrentBatsmen.Partnership){
                                    Html += '<p><span>' + BattingTeam.CurrentBatsmen.Partnership.Score + '</span></p>';
                                    Html += '<p>(' + BattingTeam.CurrentBatsmen.Partnership.Balls + ')</p>';
                                }
                            Html += '</div>';
                        }
                        Count++;
                    });
                    if(!HideActions && !HideActionsBowler && ActiveCount > 0 && Settings.Status != 'Complete'){
                        Html += '<div class="actions">';
                            Html += '<div><div class="dot bg-white">Dot</div></div>';
                            Html += '<div><div class="runs bg-green">Runs</div></div>';
                            Html += '<div><div class="extra bg-yellow">Extra</div></div>';
                            Html += '<div><div class="wicket bg-red">Wicket</div></div>';
                        Html += '</div>';
                    }
                Html += '</div>';
                /* Batsmen */

                Html += HtmlBowlers;

                Html += '<div class="buttons">';
                    if(BowlingTeam.CurrentBowlers && Settings.Status != 'Complete'){
                        Html += '<a class="btn-primary btn-block add-bowler" data-matchid="' + MatchID + '" data-teamid="' + BowlingTeam.Details.id + '">Add Bowler</a>';
                    }
                    Html += '<a class="btn-primary btn-block scorecard">View Scorecard</a>';
                Html += '</div>';
            }

            $('[data-page="view-match"] #team-history .swiper-slide').html(Html);
            
            $('.scorecard-detail').on('click',function(){
                StarterTeam($(this).attr('data-matchid'), $(this).attr('data-teamid'));
            });
            
            $('.refresh-game-data').on('click',function(e){
                e.preventDefault();
                e.stopPropagation();
                GetMatch($(this).attr('data-matchid'));
            });

            if(Settings.Status != 'Complete'){
                $('.non-active-batsmen').on('click',function(){
                    $.ajax({
                        type: "POST",
                        dataType: 'jsonp',
                        url : "http://groot.dedicated.co.za/cric-it/api",
                        data : 'wrapper=match&type=ActiveBatsman&MatchID=' + $(this).attr('data-matchid') + '&UserID=' + $(this).attr('data-playerid') + '&_token=t8Y]^RhRe,xp@Xfk76%$',
                        success : function(data){
                            GetMatch(MatchID);
                            mainView.router.loadPage({
                                url: 'view-match.html',
                                query: {
                                    MatchID: MatchID
                                }
                            });
                            myApp.closeModal();
                        }
                    },"json");
                });
            
                $('.no-batsmen').on('click',function(){
                    var MatchID = $(this).attr('data-matchid');
                    var TeamID = $(this).attr('data-teamid');
                    var Batsman = $(this).attr('data-rel');
                    var BatsmenList = BattingTeam.Players;
                    var popupHTML = ''+
                    '<div class="popup popup-addplayer">'+
                        '<div class="content-block">'+
                            '<div class="close">'+
                                '<a href="#" class="close-popup"><i class="fa fa-times-circle" aria-hidden="true"></i></a>'+
                            '</div>'+
                            '<div class="player-body">'+
                                '<p>Choose your</p>'+
                                '<h2>Batsman</h2>'+
                                '<div class="list-block">'+
                                    '<ul>';
                                    $.each(BatsmenList,function(k,v){
                                        if($('#batsmen-1').val() == v.Profile.User.id || $('#batsmen-2').val() == v.Profile.User.id || v.Score.Batting.Status){

                                        }else{
                                            popupHTML += '<li>'+
                                                '<a href="#" class="item-link choose-batsman" data-matchid="' + MatchID + '" data-teamid="' + TeamID + '" data-playerid="' + v.Profile.User.id + '" data-batsman="' + Batsman + '" >'+
                                                    '<div class="item-content">'+
                                                        '<div class="item-media">'+
                                                            '<img src="' + v.Profile.User.ProfileIMG + '"/>'+
                                                        '</div>'+
                                                        '<div class="item-inner">'+
                                                            '<div class="item-title">' + v.Profile.User.FirstName + ' ' + v.Profile.User.LastName + '</div>'+
                                                        '</div>'+
                                                    '</div>'+
                                                '</a>'+
                                            '</li>';
                                        }
                                    })
                                    popupHTML += '</ul>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                    '</div>';
                    myApp.popup(popupHTML, true);
                    $('.choose-batsman').on('click',function(){
                        var MatchID = $(this).attr('data-matchid');
                        $.ajax({
                            type: "POST",
                            dataType: 'jsonp',
                            url : "http://groot.dedicated.co.za/cric-it/api",
                            data : 'wrapper=match&type=ActivateBatsman&MatchID=' + MatchID + '&TeamID=' + TeamID + '&UserID=' + $(this).attr('data-playerid') + '&Batsman=' + $(this).attr('data-batsman') + '&_token=t8Y]^RhRe,xp@Xfk76%$',
                            success : function(data){
                                GetMatch(MatchID);
                                mainView.router.loadPage({
                                    url: 'view-match.html',
                                    query: {
                                        MatchID: MatchID
                                    }
                                });
                                myApp.closeModal();
                            }
                        },"json");
                    }); 
                });

                $('.no-bowlers, .add-bowler').on('click',function(){
                    var MatchID = $(this).attr('data-matchid');
                    var TeamID = $(this).attr('data-teamid');
                    var Bowler = $(this).attr('data-rel');
                    var BowlerList = BowlingTeam.Players;
                    var popupHTML = ''+
                    '<div class="popup popup-addplayer">'+
                        '<div class="content-block">'+
                            '<div class="close">'+
                                '<a href="#" class="close-popup"><i class="fa fa-times-circle" aria-hidden="true"></i></a>'+
                            '</div>'+
                            '<div class="player-body">'+
                                '<p>Choose your</p>'+
                                '<h2>bowler</h2>'+
                                '<div class="list-block">'+
                                    '<ul>';
                                    $.each(BowlerList,function(k,v){
                                        var ShowMe = true;
                                        $('.current-bowlers').each(function(){
                                            if($(this).val() == v.Profile.User.id){
                                                ShowMe = false;
                                            }
                                        });
                                        if(ShowMe){
                                            popupHTML += '<li>'+
                                                '<a href="#" class="item-link choose-bowler" data-matchid="' + MatchID + '" data-playerid="' + v.Profile.User.id + '" data-teamid="' + TeamID + '" >'+
                                                    '<div class="item-content">'+
                                                        '<div class="item-media">'+
                                                            '<img src="' + v.Profile.User.ProfileIMG + '"/>'+
                                                        '</div>'+
                                                        '<div class="item-inner">'+
                                                            '<div class="item-title">' + v.Profile.User.FirstName + ' ' + v.Profile.User.LastName + '</div>'+
                                                        '</div>'+
                                                    '</div>'+
                                                '</a>'+
                                            '</li>';
                                        }
                                    })
                                    popupHTML += '</ul>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                    '</div>';
                    myApp.popup(popupHTML, true);

                    $('.choose-bowler').on('click',function(){
                        var MatchID = $(this).attr('data-matchid');
                        $.ajax({
                            type: "POST",
                            dataType: 'jsonp',
                            url : "http://groot.dedicated.co.za/cric-it/api",
                            data : 'wrapper=match&type=ActivateBowler&MatchID=' + MatchID + '&UserID=' + $(this).attr('data-playerid') + '&TeamID=' + $(this).attr('data-teamid') + '&_token=t8Y]^RhRe,xp@Xfk76%$',
                            success : function(data){
                                GetMatch(MatchID);
                                mainView.router.loadPage({
                                    url: 'view-match.html',
                                    query: {
                                        MatchID: MatchID
                                    }
                                });
                                myApp.closeModal();
                            }
                        },"json");
                    });
                });

                $('.non-active-bowler').on('click',function(){
                    var TeamID = $('#bowling-teamid').val();
                    $.ajax({
                        type: "POST",
                        dataType: 'jsonp',
                        url : "http://groot.dedicated.co.za/cric-it/api",
                        data : 'wrapper=match&type=ActiveBowler&MatchID=' + $(this).attr('data-matchid') + '&UserID=' + $(this).attr('data-playerid') + '&TeamID=' + TeamID + '&_token=t8Y]^RhRe,xp@Xfk76%$',
                        success : function(data){
                            GetMatch(MatchID);
                            mainView.router.loadPage({
                                url: 'view-match.html',
                                query: {
                                    MatchID: MatchID
                                }
                            });
                            myApp.closeModal();
                        }
                    },"json");
                });

                $('.complete-over').on('click',function(){
                    preloader('', 'body');
                    $.ajax({
                        type: "POST",
                        dataType: 'jsonp',
                        url : "http://groot.dedicated.co.za/cric-it/api",
                        data : 'wrapper=score&type=CompleteOver&OverID=' + $(this).attr('data-overid') + '&_token=t8Y]^RhRe,xp@Xfk76%$',
                        success : function(data){
                            GetMatch(MatchID);
                            mainView.router.loadPage({
                                url: 'view-match.html',
                                query: {
                                    MatchID: MatchID
                                }
                            });
                            myApp.closeModal();
                            preloaderRemove();
                        }
                    },"json");
                });
            }

            Scoring(MatchID, $('#active-bowler').val(), $('#active-batsmen').val(), $('#partner-batsmen').val(), $('#current-over').val(), $('#current-over-id').val(), BowlingTeam.Players);
            Scorecard(MatchID);
            preloaderRemove();
        },
        error : function(data){
            preloaderRemove();
            myApp.addNotification({
                title: 'Error',
                message: 'Oops. Something went wrong!',
                hold: 5000
            });
        }
    },"json");
}

function Scoring(MatchID = false, BowlerID = false, BatsmanID = false, PartnerID = false, CurrentOver = false, CurrentOverID = false, BowlingTeam = false){
    $('.dot').on('click',function(){
        Dot(MatchID, BowlerID, BatsmanID, PartnerID, CurrentOver, CurrentOverID);
    });
    $('.runs').on('click',function(){
        Runs(MatchID, BowlerID, BatsmanID, PartnerID, CurrentOver, CurrentOverID);
    });
    $('.extra').on('click',function(){
        Extras(MatchID, BowlerID, BatsmanID, PartnerID, CurrentOver, CurrentOverID);
    });
    $('.wicket').on('click',function(){
        Wicket(MatchID, BowlerID, BatsmanID, PartnerID, CurrentOver, CurrentOverID, BowlingTeam);
    });
}

function Wicket(MatchID = false, BowlerID = false, BatsmanID = false, PartnerID = false, CurrentOver = false, CurrentOverID = false, BowlingTeam = false){
    var BowlersValues = [];
    var BowlersDisplayValues = [];
    $.each(BowlingTeam,function(k,v){
        BowlersValues.push(v.Profile.User.id);
        BowlersDisplayValues.push(v.Profile.User.FirstName);
    });
    BowlersValues.push(0);
    BowlersDisplayValues.push('');

    var WicketPicker = myApp.picker({
        input: '.wicket',
        rotateEffect: true,
        toolbarTemplate: 
            '<div class="toolbar">' +
                '<div class="toolbar-inner">' +
                    '<div class="left">' +
                        '<a href="#" class="link close-picker">Close</a>' +
                    '</div>' +
                    '<div class="right">' +
                        '<a href="#" class="link close-picker process">Process</a>' +
                    '</div>' +
                '</div>' +
            '</div>',
        cols: [
            {
                textAlign: 'left',
                values: ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15']
            },
            {
                textAlign: 'left',
                values: ['Caught', 'LBW', 'Bowled', 'Hit Wicket', 'Run Out', 'Timed Out', 'Handled Ball', 'Hit Ball Twice', 'Obstruction', 'Retired Out', 'Stumped'],
                onChange: function (picker, wickettype) {
                    if(wickettype == 'LBW' || wickettype == 'Bowled'){
                        picker.cols[2].setValue(BowlerID);
                    }else if(wickettype == 'Hit Wicket' || wickettype == 'Timed Out' || wickettype == 'Handled Ball' || wickettype == 'Hit Ball Twice' || wickettype == 'Retired Out'){
                        picker.cols[2].setValue(0);
                    }
                }
            },
            {
                textAlign: 'right',
                values: BowlersValues,
                displayValues: BowlersDisplayValues
            }
        ]
    });
    WicketPicker.open();
    $('.close-picker.process').on('click',function(){
        preloader('', 'body');
        $.ajax({
            type: "POST",
            dataType: 'jsonp',
            url : "http://groot.dedicated.co.za/cric-it/api",
            data : 'wrapper=score&type=Wicket&Value=' + WicketPicker.value[0] + '&WicketType=' + WicketPicker.value[1] + '&WicketFielder=' + WicketPicker.value[2] + '&BattingTeamID=' + $('#batting-teamid').val() + '&BowlingTeamID=' + $('#bowling-teamid').val() + '&MatchID=' + MatchID + '&BowlerID=' + BowlerID + '&PartnerID=' + PartnerID + '&BatsmanID=' + BatsmanID + '&CurrentOver=' + CurrentOver + '&CurrentOverID=' + CurrentOverID + '&_token=t8Y]^RhRe,xp@Xfk76%$',
            success : function(data){
                GetMatch(MatchID);
                mainView.router.loadPage({
                    url: 'view-match.html',
                    query: {
                        MatchID: MatchID
                    }
                });
                myApp.closeModal();
                preloaderRemove();
            }
        },"json");
    });
}

function Extras(MatchID = false, BowlerID = false, BatsmanID = false, PartnerID = false, CurrentOver = false, CurrentOverID = false){
    var ExtrasPicker = myApp.picker({
        input: '.extra',
        rotateEffect: true,
        toolbarTemplate: 
            '<div class="toolbar">' +
                '<div class="toolbar-inner">' +
                    '<div class="left">' +
                        '<a href="#" class="link close-picker">Close</a>' +
                    '</div>' +
                    '<div class="right">' +
                        '<a href="#" class="link close-picker process">Process</a>' +
                    '</div>' +
                '</div>' +
            '</div>',
        cols: [
            {
                textAlign: 'left',
                values: ['Wide', 'No Ball', 'Bye', 'Leg Bye', 'Penalty Runs']
            },
            {
                textAlign: 'right',
                values: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15']
            }
        ]
    });
    ExtrasPicker.open();
    $('.close-picker.process').on('click',function(){
        preloader('', 'body');
        $.ajax({
            type: "POST",
            dataType: 'jsonp',
            url : "http://groot.dedicated.co.za/cric-it/api",
            data : 'wrapper=score&type=Extras&ExtraType=' + ExtrasPicker.value[0] + '&Value=' + ExtrasPicker.value[1] + '&BattingTeamID=' + $('#batting-teamid').val() + '&BowlingTeamID=' + $('#bowling-teamid').val() + '&MatchID=' + MatchID + '&BowlerID=' + BowlerID + '&PartnerID=' + PartnerID + '&BatsmanID=' + BatsmanID + '&CurrentOver=' + CurrentOver + '&CurrentOverID=' + CurrentOverID + '&_token=t8Y]^RhRe,xp@Xfk76%$',
            success : function(data){
                GetMatch(MatchID);
                mainView.router.loadPage({
                    url: 'view-match.html',
                    query: {
                        MatchID: MatchID
                    }
                });
                myApp.closeModal();
                preloaderRemove();
            }
        },"json");

    });
}

function Dot(MatchID = false, BowlerID = false, BatsmanID = false, PartnerID = false, CurrentOver = false, CurrentOverID = false){
    preloader('', 'body');
    $.ajax({
        type: "POST",
        dataType: 'jsonp',
        url : "http://groot.dedicated.co.za/cric-it/api",
        data : 'wrapper=score&type=Dot&MatchID=' + MatchID + '&BattingTeamID=' + $('#batting-teamid').val() + '&BowlingTeamID=' + $('#bowling-teamid').val() + '&BowlerID=' + BowlerID + '&PartnerID=' + PartnerID + '&BatsmanID=' + BatsmanID + '&CurrentOver=' + CurrentOver + '&CurrentOverID=' + CurrentOverID + '&_token=t8Y]^RhRe,xp@Xfk76%$',
        success : function(data){
            GetMatch(MatchID);
            mainView.router.loadPage({
                url: 'view-match.html',
                query: {
                    MatchID: MatchID
                }
            });
            myApp.closeModal();
            preloaderRemove();
        }
    },"json");
}

function Runs(MatchID = false, BowlerID = false, BatsmanID = false, PartnerID = false, CurrentOver = false, CurrentOverID = false){
    var RunsPicker = myApp.picker({
        input: '.runs',
        rotateEffect: true,
        toolbarTemplate: 
            '<div class="toolbar">' +
                '<div class="toolbar-inner">' +
                    '<div class="left">' +
                        '<a href="#" class="link close-picker">Close</a>' +
                    '</div>' +
                    '<div class="right">' +
                        '<a href="#" class="link close-picker process">Process</a>' +
                    '</div>' +
                '</div>' +
            '</div>',
        cols: [
            {
                textAlign: 'center',
                values: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15']
            }
        ]
    });
    RunsPicker.open();
    $('.close-picker.process').on('click',function(){
        preloader('', 'body');
        $.ajax({
            type: "POST",
            dataType: 'jsonp',
            url : "http://groot.dedicated.co.za/cric-it/api",
            data : 'wrapper=score&type=Runs&Value=' + RunsPicker.value[0] + '&BattingTeamID=' + $('#batting-teamid').val() + '&BowlingTeamID=' + $('#bowling-teamid').val() + '&MatchID=' + MatchID + '&BowlerID=' + BowlerID + '&PartnerID=' + PartnerID + '&BatsmanID=' + BatsmanID + '&CurrentOver=' + CurrentOver + '&CurrentOverID=' + CurrentOverID + '&_token=t8Y]^RhRe,xp@Xfk76%$',
            success : function(data){
                GetMatch(MatchID);
                mainView.router.loadPage({
                    url: 'view-match.html',
                    query: {
                        MatchID: MatchID
                    }
                });
                myApp.closeModal();
                preloaderRemove();
            }
        },"json");
    });
}

function GetMatches(){
    preloader('', 'body');
    $.ajax({
        type: "POST",
        dataType: 'jsonp',
        url : "http://groot.dedicated.co.za/cric-it/api",
        data : 'wrapper=match&type=GetMatches&_token=t8Y]^RhRe,xp@Xfk76%$',
        success : function(data){
            var Html = '';
            $.each(data,function(a,b){
                Html += '<div class="content-block-title">' + a + ' Games</div>';
                Html += '<div class="list-block">';
                    Html += '<div class="list-group">';
                        Html += '<ul>';
                            $.each(b,function(c,d){
                                Html += '<li class="item-content">';
                                    Html += '<a href = "#" data-rel="' + d.id + '" class = "item-link view-match">';
                                        Html += '<div class="item-inner">';
                                            Html += '<div class="item-title"><img src="'+d.Team1.Logo+'"/><div class="teamnames">'+d.Team1.Name+' vs '+d.Team2.Name+'</div><img src="'+d.Team2.Logo+'"/></div>';
                                        Html += '</div>';
                                    Html += '</a>';
                                Html += '</li>';
                            });
                            Html += '</li>';
                        Html += '</ul>';
                    Html += '</div>';
                Html += '</div>';
            });
            
            $('[data-page="view-matches"].page h1').after(Html);
            preloaderRemove();
            $('.view-match').on('click',function(){
                mainView.router.loadPage({
                    url: 'view-match.html',
                    query: {
                        MatchID: $(this).attr('data-rel')
                    }
                });
            });
        },
        error : function(data){
            preloaderRemove();
            myApp.addNotification({
                title: 'Error',
                message: 'Oops. Something went wrong!',
                hold: 5000
            });
        }
    },"json");
}
////////////// Matches ///////////////////////

////////////// Teams ///////////////////////
function AddTeam(){
    $('#AddTeam button').on('click',function(){
        var formname = 'AddTeam';
        if(validation(formname)){   
            /* Sign In */
            preloader('', 'body');
            $.ajax({
                type: "POST",
                dataType: 'jsonp',
                url : "http://groot.dedicated.co.za/cric-it/api",
                data : 'wrapper=teams&type=AddTeam&Name=' + $('input[name="Name"]').val() + '&Location=' + $('select[name="Location"]').val() + '&Format=' + $('select[name="Format"]').val() + '&Coach=' + $('input[name="Coach"]').val() + '&_token=t8Y]^RhRe,xp@Xfk76%$',
                success : function(data){
                    if(data.status == 'true'){
                        mainView.router.loadPage({
                            url: 'view-team.html',
                            query: {
                                TeamID: data.result
                            }
                        });
                    }else{
                        preloaderRemove();
                        myApp.addNotification({
                            title: 'Error',
                            message: 'Oops! ' + $('input[name="Name"]').val() + ' already exists',
                            hold: 5000
                        });
                        mainView.router.loadPage({
                            url: 'view-team.html',
                            query: {
                                TeamID: data.result
                            }
                        });
                    }
                    preloaderRemove();
                },
                error : function(data){
                    preloaderRemove();
                    myApp.addNotification({
                        title: 'Error',
                        message: 'Oops! Something went wrong, please try again later.',
                        hold: 5000
                    });
                }
            },"json");
            return false;
        }    
        return false;
    });
}

function Teams(Which = false, VariableWhich = false, UpdateField = false, Team1 = false, Team2 = false){
    var storedData = myApp.formGetData('UserDetails');
    myApp.closeModal();
    myApp.popup();
    if (Which == 'club-cricket'){
        var UrlVar = 'Clubs';
        var SelectName = 'club-provinces';
    }else if (Which == 'school-cricket'){
        var UrlVar = 'Schools';
        var SelectName = 'school-provinces';
    }else if (Which == 'social-cricket'){
        var UrlVar = 'Social';
        var SelectName = 'social-provinces';
    }
    var Html = '<div class="list-block"><ul>';
    $.ajax({
        type: "POST",
        dataType: 'jsonp',
        url : "http://groot.dedicated.co.za/cric-it/api",
        data : 'wrapper=teams&type=' + UrlVar + '&UserID=' + storedData.User.id + '&_token=t8Y]^RhRe,xp@Xfk76%$',
        success : function(data){
            $.each(data,function(k,v){
                Html += '<li>';
                    Html += '<a href="#" class="item-link smart-select" data-searchbar="true" data-searchbar-placeholder="Search ' + UrlVar + '">';
                        Html += '<select name="' + SelectName + '" multiple>';
                            $.each(v,function(a,b){
                                var OptionSelected = b.Selected;
                                if(VariableWhich == 'match'){
                                    if(b.id == Team1 || b.id == Team2){
                                        OptionSelected = 'selected';
                                    }else{
                                        OptionSelected = '';
                                    }
                                }
                                Html += '<option value="' + b.id + '" ' + OptionSelected + ' data-option-image="' + b.Logo + '">' + b.Name + '</option>';
                            });
                        Html += '</select>';

                        Html += '<div class="item-content">';
                            Html += '<div class="item-inner">';
                                Html += '<div class="item-title">' + k + '</div>';
                            Html += '</div>';
                        Html += '</div>';
                    Html += '</a>';
                Html += '</li>';
            });
            Html += '</ul></div><div class="add-team">Can\'t find your team? Click here to add it.</div>';
            
            $('#' + Which).html(Html);

            $('.add-team').on('click',function(){
                myApp.closeModal();
                mainView.router.loadPage('add-team.html');
            });

            if(VariableWhich == 'user'){
                $('select[name="' + SelectName + '"]').on('change',function () {
                    preloader('', 'body');
                    UpdateUserTeam(this.value);
                });
            }else if(VariableWhich == 'match'){
                $('select[name="' + SelectName + '"]').on('change',function () {
                    if(UpdateField == 'Team1'){
                        Team1 = {id:this.value,name:$(this).find("option:selected").text()};
                    }
                    if(UpdateField == 'Team2'){
                        Team2 = {id:this.value,name:$(this).find("option:selected").text()};
                    }
                    myApp.closeModal();
                    mainView.router.loadPage({
                            url: 'add-match.html',
                            query: {
                                Team1: Team1,
                                Team2: Team2
                            }
                        });
                    mainView.router.loadPage('add-match.html');
                });
            }else if(VariableWhich == 'tournament'){
                $('select[name="' + SelectName + '"]').on('change',function () {
                    preloader('', 'body');
                    AddTeamToTournament(UpdateField, this.value);
                });
            }
        },
        error : function(data){
            
        }
    },"json");
}

function GetTeams(){
    preloader('', 'body');
    $.ajax({
        type: "POST",
        dataType: 'jsonp',
        url : "http://groot.dedicated.co.za/cric-it/api",
        data : 'wrapper=teams&type=GetTeams&_token=t8Y]^RhRe,xp@Xfk76%$',
        success : function(data){
            var Html = '';
            $.each(data,function(a,b){
                Html += '<div class="content-block-title">' + a + ' Cricket</div>';
                    Html += '<div class="list-block medialist">';
                        Html += '<ul>';
                            $.each(b,function(c,d){
                                var SelectName = slug(c);
                                Html += '<li>';
                                    Html += '<a href = "#" data-searchbar = "true" data-searchbar-placeholder = "Search ' + c + ' - ' + a + ' Cricket" class = "item-link smart-select">';
                                        Html += '<select name="' + SelectName + '">';
                                            Html += '<option value=""selected disabled></option>';
                                            $.each(d,function(e,f){
                                                Html += '<option value="' + f.id + '" data-option-image="' + f.Logo + '">' + f.Name + '</option>';
                                            });
                                        Html += '</select>';
                                        Html += '<div class = "item-content">';
                                            Html += '<div class = "item-inner">';
                                                Html += '<div class = "item-title">' + c + '</div>';
                                            Html += '</div>';
                                        Html += '</div>';
                                    Html += '</a>';
                                Html += '</li>';
                            });
                        Html += '</ul>';
                    Html += '</div>';
                Html += '</div>';
            });
            $('[data-page="view-teams"].page h1').after(Html);
            preloaderRemove();
            $('[data-page="view-teams"].page select').on('change',function () {
                mainView.router.loadPage({
                    url: 'view-team.html',
                    query: {
                        TeamID: this.value
                    }
                });
            });
        },
        error : function(data){
            preloaderRemove();
            myApp.addNotification({
                title: 'Error',
                message: 'Oops. Something went wrong!',
                hold: 5000
            });
        }
    },"json");
}

function ViewTeam(TeamID = false, Refresh = false){
    $TeamID = TeamID;
    preloader('', 'body');
    if(Refresh){
        $('.team-detail').html('');
    }
    var storedData = myApp.formGetData('UserDetails');
    $.ajax({
        type: "POST",
        dataType: 'jsonp',
        url : "http://groot.dedicated.co.za/cric-it/api",
        data : 'wrapper=teams&type=ViewTeam&TeamID=' + TeamID + '&_token=t8Y]^RhRe,xp@Xfk76%$',
        success : function(data){
            $('[data-page="view-team"].page #team-details h1').html(data.Team.Name);
            $('[data-page="view-team"].page #team-details .location').html(data.Team.Location);
            $('[data-page="view-team"].page #team-details .format').html(data.Team.Format);
            $('[data-page="view-team"].page #team-details .format').addClass(data.Team.Format);
            $('[data-page="view-team"].page #team-details .logo img').attr('src',data.Team.Logo);
            var Html = '';
            var Coaches = '';
            if(data.Coaches.length > 0){
                $.each(data.Coaches,function(k,v){
                    Coaches += v.Name + ', ';
                });
            }else{
                Coaches = '<em>No coaches available</em>';
            }
            Html = '<div class="content-block-title">Coaches: <span>' + Coaches.replace(/, +$/,'') + '</span>';
            if(storedData.User.Role == 'administrator'){
                if(data.Coaches.length > 0){
                    Html += '<i class="fa fa-pencil fa-fw add-coach-to-team" data-teamname="' + data.Team.Name + '" data-coaches="' + Coaches.replace(/, +$/,'') + '" aria-hidden="true"></i>';
                }else{
                    Html += '<i class="fa fa-plus fa-fw add-coach-to-team" data-teamname="' + data.Team.Name + '" aria-hidden="true"></i>';
                }
            }
            Html += '</div>';
            Html += '<div class="content-block-title">Players: ';
            var SwipeOut = '';
            var SwipeOutContent = '';
            if(storedData.User.Role == 'administrator'){
                Html += '<i class="fa fa-plus fa-fw add-player-to-team" data-teamname="' + data.Team.Name + '" aria-hidden="true"></i>';
                SwipeOut = 'swipeout';
                SwipeOutContent = 'swipeout-content';
            }
            Html += '</div>';
            if(data.Players.length > 0){
                Html += '<div class="list-block medialist">';
                    Html += '<ul>';
                        $.each(data.Players,function(k,v){
                            Html += '<li class="' + SwipeOut + '" data-rel="' + v.id + '">';
                                Html += '<div class = "' + SwipeOutContent + ' item-content">';
                                    Html += '<div class = "item-inner">';
                                        Html += '<div class = "item-media">';
                                            Html += '<img src="' + v.ProfileIMG + '"/>';
                                        Html += '</div>';
                                        Html += '<div class = "item-title">';
                                            Html += v.FirstName + ' ' + v.LastName;
                                        Html += '</div>';
                                    Html += '</div>';
                                Html += '</div>';
                                if(storedData.User.Role == 'administrator'){
                                    Html += '<div class="swipeout-actions-right">';
                                        Html += '<a href="#" class="swipeout-delete" id="' + v.id + '" data-confirm="Are you sure want to delete ' + v.FirstName + '?" data-confirm-title="Delete?">Delete</a>';
                                    Html += '</div>';
                                }
                            Html += '</li>';
                        });
                    Html += '</ul>';
                Html += '</div>';
            }else{
                Html += '<p><em>No players available</em></p>';
            }
            $('#team-details .team-detail ').html(Html);
            preloaderRemove();
            $$('.swipeout').on('deleted', function (e) {
                DeleteUserFromTeam($(this).attr('data-rel'), TeamID);
            });
            AddPlayerToTeam(TeamID);
            AddCoachToTeam(TeamID);
            Scorecard(1);
        },
        error : function(data){
            preloaderRemove();
            myApp.addNotification({
                title: 'Error',
                message: 'Oops. Something went wrong!',
                hold: 5000
            });
        }
    },"json");
}

function StarterTeam(MatchID = false, TeamID = false){
    $.ajax({
        type: "POST",
        dataType: 'jsonp',
        url : "http://groot.dedicated.co.za/cric-it/api",
        data : 'wrapper=match&type=StarterTeam&TeamID=' + TeamID + '&MatchID=' + MatchID + '&_token=t8Y]^RhRe,xp@Xfk76%$',
        success : function(data){
            GetMatch(MatchID);
            mainView.router.loadPage({
                url: 'view-match.html',
                query: {
                    MatchID: MatchID
                }
            });
        },
        error : function(data){
            
        }
    },"json");
}

function AddCoachToTeam(TeamID = false){
    $('.add-coach-to-team').on('click',function(){
        var popupHTML = ''+
        '<div class="popup popup-addcoach">'+
            '<div class="content-block">'+
                '<div class="close">'+
                    '<a href="#" class="close-popup"><i class="fa fa-times-circle" aria-hidden="true"></i></a>'+
                '</div>'+
                '<div class="player-body">'+
                    '<p>Add a coach to</p>'+
                    '<h2>' + $(this).attr('data-teamname') + '</h2>'+
                    '<p class="instructions">Please enter the coach\'s name.</p>'+
                    '<small>(To enter more than one coach, seperate by comma)</small>'+
                    '<form id="AddCoachToTeam">'+
                       '<div class="form-element">'+
                            '<input id="Coaches" name="Coaches" type="text" placeholder="Coach\'s name" data-required="Please fill in the coach\'s name" required value="' + $(this).attr('data-coaches') + '"/>'+
                        '</div>'+
                        '<button class="btn-primary round">'+
                            '<i class="fa fa-user-plus fa-fw" aria-hidden="true"></i>'+
                        '</button>'+
                    '</form>'+
                '</div>'+
            '</div>'+
        '</div>';
        myApp.popup(popupHTML, true);
        $('.popup-addcoach button').on('click',function(e){
            e.preventDefault();
            var formname = 'AddCoachToTeam';
            if(validation(formname)){
                preloader('', 'body');
                $.ajax({
                    type: "POST",
                    dataType: 'jsonp',
                    url : "http://groot.dedicated.co.za/cric-it/api",
                    data : 'wrapper=teams&type=AddCoach&Coaches=' + $('input[name="Coaches"]').val() + '&TeamID=' + TeamID + '&_token=t8Y]^RhRe,xp@Xfk76%$',
                    success : function(data){
                        if(data.status == 'true'){
                           ViewTeam(TeamID, true);     
                           myApp.closeModal();        
                        }else{
                           myApp.addNotification({
                                title: 'Error',
                                message: data.result,
                                hold: 5000
                            });
                        }
                        preloaderRemove();
                    },
                    error : function(data){
                        preloaderRemove();
                        myApp.addNotification({
                            title: 'Error',
                            message: 'Oops. Something went wrong!',
                            hold: 5000
                        });
                    }
                },"json");
                return false;
            }
        });
    });
}

function FindFormats(Which = false, Team1 = false, Team2 = false){
    $('.team-select, .add-team-to-tournament').on('click',function(){
        var UpdateField = $(this).attr('data-rel');
        var buttons1 = [{
                text: 'Select the format',
                label: true
            },{
                text: 'Club Cricket',
                onClick: function () {
                    myApp.closeModal('.modal-overlay');
                    myApp.closeModal('.popup');
                    mainView.router.loadPage({
                        url: 'club-cricket.html',
                        query: {
                            Which: Which,
                            UpdateField: UpdateField,
                            Team1: Team1,
                            Team2: Team2
                        }
                    });
                }
            },{
                text: 'School Cricket',
                onClick: function () {
                    myApp.closeModal('.modal-overlay');
                    myApp.closeModal('.popup');
                    mainView.router.loadPage({
                        url: 'school-cricket.html',
                        query: {
                            Which: Which,
                            UpdateField: UpdateField,
                            Team1: Team1,
                            Team2: Team2
                        }
                    });
                }
            },{
                text: 'Social Cricket',
                onClick: function () {
                    myApp.closeModal('.modal-overlay');
                    myApp.closeModal('.popup');
                    mainView.router.loadPage({
                        url: 'social-cricket.html',
                        query: {
                            Which: Which,
                            UpdateField: UpdateField,
                            Team1: Team1,
                            Team2: Team2
                        }
                    });
                }
            }
        ];
        var buttons2 = [
        {
            text: 'Cancel',
            color: 'red',
            onClick: function () {
                myApp.closeModal('.modal-overlay');
                myApp.closeModal('.popup');
            }
        }
        ];
        var groups = [buttons1, buttons2];
        myApp.actions(groups);
    });
}

function onTeamImageSuccess(imageURI) {
    preloader('', 'body');
    var options = new FileUploadOptions();
    options.fileKey="file";
    options.fileName=imageURI.substr(imageURI.lastIndexOf('/')+1);
    options.mimeType="image/jpeg";
    options.httpMethod = 'POST';
    var params = new Object();
    params.TeamID = $TeamID;
    params.wrapper = 'teams';
    params.type = 'TeamImageUpdate';

    options.params = params;
    options.chunkedMode = false;
    options.headers = {
        Connection: "close"
    }
    var ft = new FileTransfer();
    ft.upload(imageURI, "http://groot.dedicated.co.za/cric-it/api/index.php", UploadWin, UploadFail, options);
    var image = document.getElementById('team-img');
    image.src = imageURI;
    preloaderRemove();
    return false;
}

function onTeamImageFail(message) {
    console.log('Failed because: ' + message);
}
////////////// Teams ///////////////////////

////////////// Scorecard ///////////////////////
function Scorecard(MatchID = false){
    $('#team-history .scorecard').on('click',function(){
        preloader('', 'body');
        var popupHTML = ''+
        '<div class="popup popup-scorecard">'+
            '<div class="content-block">'+
                '<div class="close">'+
                    '<a href="#" class="close-popup"><i class="fa fa-times-circle" aria-hidden="true"></i></a>'+
                '</div>'+
                '<div class="player-body">'+
                    '<h2>Scorecard</h2>';
        $.ajax({
            type: "POST",
            dataType: 'jsonp',
            url : "http://groot.dedicated.co.za/cric-it/api",
            data : 'wrapper=match&type=GetMatchScoreCard&MatchID=' + MatchID + '&_token=t8Y]^RhRe,xp@Xfk76%$',
            success : function(data){
                console.log(data.result);
                popupHTML += '<h1 class="result ' + data.result.Status + '">' + data.result.Result + '</h1>';
                $.each(data.result.Innings,function(k,v){
                    var BattingTeam = v.ScoreCard.BattingTeam;
                    var BowlingTeam = v.ScoreCard.BowlingTeam;
                    popupHTML += ''+
                    '<div class="heading">'+
                        '<div class="team">' + BattingTeam.Details.Name + '</div>'+
                        '<div class="score">' + BattingTeam.Score + '-' + BattingTeam.Wickets + '</div>'+
                        '<div class="innings">1st Innings</div>'+
                    '</div>'+
                    '<div class="data-table">'+
                          '<table class="batsmen">'+
                            '<thead>'+
                              '<tr>'+
                                '<th class="label-cell">Batsmen</th>'+
                                '<th class="numeric-cell"></th>'+
                                '<th class="numeric-cell">R</th>'+
                                '<th class="numeric-cell">B</th>'+
                              '</tr>'+
                            '</thead>'+
                            '<tbody>';
                            $.each(BattingTeam.Players,function(a,b){
                                var Wicket = Runs = Balls = '';
                                var Class = 'in-active';
                                if(b.Score.Batting.Status){
                                    Wicket = b.Score.Batting.Status.Wicket;
                                    Class = '';
                                    if(Wicket == 'Not Out'){
                                        Class = 'not-out';
                                    }
                                    Runs = b.Score.Batting.Runs;
                                    Balls = b.Score.Batting.Balls;
                                    popupHTML += ''+
                                      '<tr class="' + Class + '">'+
                                        '<td class="label-cell">' + b.Profile.User.FirstName + ' ' + b.Profile.User.LastName + '</td>'+
                                        '<td class="tablet-only">' + Wicket + '</td>'+
                                        '<td class="numeric-cell">' + Runs + '</td>'+
                                        '<td class="numeric-cell">' + Balls + '</td>'+
                                      '</tr>';
                                }
                            });

                            $.each(BattingTeam.Players,function(a,b){
                                var Wicket = Runs = Balls = '';
                                var Class = 'in-active';
                                if(!b.Score.Batting.Status){
                                    popupHTML += ''+
                                      '<tr class="' + Class + '">'+
                                        '<td class="label-cell">' + b.Profile.User.FirstName + ' ' + b.Profile.User.LastName + '</td>'+
                                        '<td class="tablet-only">' + Wicket + '</td>'+
                                        '<td class="numeric-cell">' + Runs + '</td>'+
                                        '<td class="numeric-cell">' + Balls + '</td>'+
                                      '</tr>';
                                }
                            });
                                popupHTML += ''+
                                  '<tr class="extras">'+
                                    '<td class="label-cell">Extras</td>'+
                                    '<td class="tablet-only"></td>'+
                                    '<td class="numeric-cell">' + BattingTeam.Extras + '</td>'+
                                    '<td class="numeric-cell"></td>'+
                                  '</tr>';
                            popupHTML += ''+  
                            '</tbody>'+
                        '</table>'+
                        '<table class="bowlers">'+
                            '<thead>'+
                              '<tr>'+
                                '<th class="label-cell">Bowlers</th>'+
                                '<th class="numeric-cell">O</th>'+
                                '<th class="numeric-cell">M</th>'+
                                '<th class="numeric-cell">R</th>'+
                                '<th class="numeric-cell">W</th>'+
                                '<th class="numeric-cell">NB</th>'+
                                '<th class="numeric-cell">WI</th>'+
                                '<th class="numeric-cell">OR</th>'+
                              '</tr>'+
                            '</thead>'+
                            '<tbody>';
                            $.each(BowlingTeam.CurrentBowlers,function(a,b){
                                popupHTML += ''+
                                  '<tr>'+
                                    '<td class="label-cell">' + b.Profile.User.FirstName + ' ' + b.Profile.User.LastName + '</td>'+
                                    '<td class="numeric-cell">' + b.Score.Bowling.Overs + '</td>'+
                                    '<td class="numeric-cell">' + b.Score.Bowling.Maidens + '</td>'+
                                    '<td class="numeric-cell">' + b.Score.Bowling.Runs + '</td>'+
                                    '<td class="numeric-cell">' + b.Score.Bowling.Wides + '</td>'+
                                    '<td class="numeric-cell">' + b.Score.Bowling.NoBalls + '</td>'+
                                    '<td class="numeric-cell">' + b.Score.Bowling.Wickets + '</td>'+
                                    '<td class="numeric-cell">' + b.Score.Bowling.OverRate + '</td>'+
                                  '</tr>';
                            });
                            popupHTML += ''+  
                            '</tbody>'+
                          '</table>'+
                    '</div>';
                });
                popupHTML += ''+                   
                        '</div>'+
                    '</div>'+
                '</div>';
                myApp.popup(popupHTML, true);
                preloaderRemove();
            },
            error : function(data){
                preloaderRemove();
                myApp.addNotification({
                    title: 'Error',
                    message: 'Oops. Something went wrong!',
                    hold: 5000
                });
            }
        },"json");

        
    });
}
////////////// Scorecard ///////////////////////

////////////// User ///////////////////////
function DeleteUserTeam(ID = false){
    $.ajax({
        type: "POST",
        dataType: 'jsonp',
        url : "http://groot.dedicated.co.za/cric-it/api",
        data : 'wrapper=user&type=UserDeleteTeam&ID=' + ID + '&_token=t8Y]^RhRe,xp@Xfk76%$',
        success : function(data){
            preloaderRemove();
        },
        error : function(data){
            
        }
    },"json");
}

function AddPlayerToTeam(TeamID = false){
    $('.add-player-to-team').on('click',function(){
        var popupHTML = ''+
        '<div class="popup popup-addplayer">'+
            '<div class="content-block">'+
                '<div class="close">'+
                    '<a href="#" class="close-popup"><i class="fa fa-times-circle" aria-hidden="true"></i></a>'+
                '</div>'+
                '<div class="player-body">'+
                    '<p>Add a player to</p>'+
                    '<h2>' + $(this).attr('data-teamname') + '</h2>'+
                    '<p class="instructions">Please enter the player\'s email address.</p>'+
                    '<form id="AddPlayerToTeam">'+
                       '<div class="form-element">'+
                            '<input id="EmailPlayer" name="EmailPlayer" type="email" placeholder="Email Address" data-required="Please fill in the player\'s email" required />'+
                        '</div>'+
                        '<button class="btn-primary round step1">'+
                            '<i class="fa fa-search fa-fw" aria-hidden="true"></i>'+
                        '</button>'+
                    '</form>'+
                '</div>'+
            '</div>'+
        '</div>';
        myApp.popup(popupHTML, true);
        $('.popup-addplayer .step1').on('click',function(e){
            e.preventDefault();
            var formname = 'AddPlayerToTeam';
            if(validation(formname)){
                preloader('', 'body');
                $.ajax({
                    type: "POST",
                    dataType: 'jsonp',
                    url : "http://groot.dedicated.co.za/cric-it/api",
                    data : 'wrapper=user&type=SignInUser&Email=' + $('input[name="EmailPlayer"]').val() + '&_token=t8Y]^RhRe,xp@Xfk76%$',
                    success : function(data){
                        if(data.status == 'false'){
                            var NewForm = ''+
                            '<div class="form-element">'+
                                '<input id="FirstNamePlayer" name="FirstNamePlayer" type="text" placeholder="First name" data-required="Please fill in the player\'s first name" required />'+
                            '</div>'+
                            '<div class="form-element">'+
                                '<input id="LastNamePlayer" name="LastNamePlayer" type="text" placeholder="Last name" data-required="Please fill in the player\'s last name" required />'+
                            '</div>';
                            $('.instructions').html('The email address you entered does not exist. Please register your player.');
                            $('#AddPlayerToTeam .fa').removeClass('fa-search').addClass('fa-user-plus');
                            $('#AddPlayerToTeam').prepend(NewForm);
                            $('.popup-addplayer button').removeClass('step1').addClass('step2')
                            $('.popup-addplayer .step2').on('click',function(e){
                                e.preventDefault();
                                var formname = 'AddPlayerToTeam';
                                if(validation(formname)){
                                    preloader('', 'body');
                                    $.ajax({
                                        type: "POST",
                                        dataType: 'jsonp',
                                        url : "http://groot.dedicated.co.za/cric-it/api",
                                        data : 'wrapper=user&SendMail=true&type=RegisterUser&FirstName=' + $('input[name="FirstNamePlayer"]').val() + '&LastName=' + $('input[name="LastNamePlayer"]').val() + '&Email=' + $('input[name="EmailPlayer"]').val() + '&Password=randomstr&_token=t8Y]^RhRe,xp@Xfk76%$',
                                        success : function(data){
                                            AddUserToTeam(data.result.User.id, TeamID, true);
                                            preloaderRemove();
                                            myApp.closeModal();
                                        },
                                        error : function(data){
                                            
                                        }
                                    },"json");
                                    return false;
                                }
                            });
                        }else if(data.status == 'failed'){
                            preloader('', 'body');
                            $.ajax({
                                type: "POST",
                                dataType: 'jsonp',
                                url : "http://groot.dedicated.co.za/cric-it/api",
                                data : 'wrapper=user&SendMail=true&type=GetIDUser&Email=' + $('input[name="EmailPlayer"]').val() + '&_token=t8Y]^RhRe,xp@Xfk76%$',
                                success : function(data){
                                    preloaderRemove();
                                    myApp.closeModal();
                                    if(data.status == 'false'){
                                        myApp.addNotification({
                                            title: 'Error',
                                            message: data.result,
                                            hold: 5000
                                        });
                                    }else{
                                       AddUserToTeam(data.result.User.id, TeamID, true);
                                    }
                                },
                                error : function(data){
                                    
                                }
                            },"json");
                            return false;
                        }
                        preloaderRemove();
                    },
                    error : function(data){
                        preloaderRemove();
                        myApp.addNotification({
                            title: 'Error',
                            message: 'Oops. Something went wrong!',
                            hold: 5000
                        });
                    }
                },"json");
                return false;
            }
        });
    });
}

function DeleteUserFromTeam(UserID = false, TeamID = false){
    $.ajax({
        type: "POST",
        dataType: 'jsonp',
        url : "http://groot.dedicated.co.za/cric-it/api",
        data : 'wrapper=teams&type=UserDeleteFromTeam&UserID=' + UserID + '&TeamID=' + TeamID + '&_token=t8Y]^RhRe,xp@Xfk76%$',
        success : function(data){
            preloaderRemove();
        },
        error : function(data){
            
        }
    },"json");
}

function AddUserToTeam(UserID = false, TeamID = false, RefreshPage = false){
    $.ajax({
        type: "POST",
        dataType: 'jsonp',
        url : "http://groot.dedicated.co.za/cric-it/api",
        data : 'wrapper=user&type=UserUpdateTeam&SendMail=true&TeamID=' + TeamID + '&UserID=' + UserID + '&_token=t8Y]^RhRe,xp@Xfk76%$',
        success : function(data){
            myApp.addNotification({
                title: 'Team Updates',
                message: data.result,
                hold: 5000
            });
            if(RefreshPage){
                ViewTeam(TeamID, true);
            }
            preloaderRemove();
        },
        error : function(data){
            
        }
    },"json");
}

function UpdateUserTeam(TeamID = false){
    var storedData = myApp.formGetData('UserDetails');
    $.ajax({
        type: "POST",
        dataType: 'jsonp',
        url : "http://groot.dedicated.co.za/cric-it/api",
        data : 'wrapper=user&type=UserUpdateTeam&SendMail=true&TeamID=' + TeamID + '&UserID=' + storedData.User.id + '&_token=t8Y]^RhRe,xp@Xfk76%$',
        success : function(data){
            preloaderRemove();
            myApp.addNotification({
                title: 'Team Updates',
                message: data.result,
                hold: 5000
            });
            myApp.closeModal();
            mainView.router.loadPage('home.html');
        },
        error : function(data){
            
        }
    },"json");
}

function UserTypeUpdate(Type = false, UserID = false){
    preloader('', 'body');
    $.ajax({
        type: "POST",
        dataType: 'jsonp',
        url : "http://groot.dedicated.co.za/cric-it/api",
        data : 'wrapper=user&type=UserTypeUpdate&UserID=' + UserID + '&Type=' + Type + '&_token=t8Y]^RhRe,xp@Xfk76%$',
        success : function(data){
            myApp.closeModal();
            preloaderRemove();
        },
        error : function(data){
            preloaderRemove();
            myApp.addNotification({
                title: 'Error',
                message: 'Oops! Something went wrong, please try again later.',
                hold: 5000
            });
        }
    },"json");
    return false;
}

function UserDetail(){
    var storedData = myApp.formGetData('UserDetails');
    if(storedData != null && storedData != 'undefined') {
        /*Get Data*/
            $.ajax({
                type: "POST",
                dataType: 'jsonp',
                url : "http://groot.dedicated.co.za/cric-it/api",
                data : 'wrapper=user&type=GetUser&UserID=' + storedData.User.id + '&Email=' + storedData.User.Email + '&Password=' + storedData.User.Password + '&_token=t8Y]^RhRe,xp@Xfk76%$',
                success : function(data){
                    if(data.status == 'false'){
                        SignOutAction();
                    }else{
                        var storedData = myApp.formStoreData('UserDetails', {
                            'User': data.result.User,
                            'Roles': data.result.Roles,
                            'Teams': data.result.Teams,
                            'Games': data.result.Games,
                            'Tournaments': data.result.Tournaments
                        });
                    }
                },
                error : function(data){
                    preloaderRemove();
                    myApp.addNotification({
                        title: 'Error',
                        message: 'Oops. Something went wrong!',
                        hold: 5000
                    });
                }
            },"json");
        /*Get Data*/

        storedData = myApp.formGetData('UserDetails');
        if (storedData.User.ProfileIMG != null) {
            $('#profile-img').attr('src',storedData.User.ProfileIMG);
        }
        console.log(storedData);
        $('#user-name').html(storedData.User.FirstName + ' ' + storedData.User.LastName);
        if(storedData.Teams){
            var TeamsHTML = '<p>Your Teams</p>';
            TeamsHTML += '<div class="list-block">';
                TeamsHTML += '<ul>';
                    $.each(storedData.Teams,function(k,v){
                        TeamsHTML += '<li class="swipeout" data-rel="' + v.id + '">';
                            TeamsHTML += '<div class="swipeout-content item-content">';
                                TeamsHTML += '<div class="item-media"><img src="' + v.Logo + '"/></div>';
                                TeamsHTML += '<div class="item-inner">' + v.Name + ' <small>' + v.Location + '</small></div>';
                            TeamsHTML += '</div>';
                            TeamsHTML += '<div class="swipeout-actions-right">';
                                TeamsHTML += '<a href="#" class="swipeout-view bg-blue" id="' + v.id + '">View</a>';
                                TeamsHTML += '<a href="#" class="swipeout-delete" id="' + v.id + '" data-confirm="Are you sure want to delete ' + v.Name + '?" data-confirm-title="Delete?">Delete</a>';
                            TeamsHTML += '</div>';
                        TeamsHTML += '</li>';
                    });
                TeamsHTML += '</ul>';
            TeamsHTML += '</div>';
            TeamsHTML += '<a class="team-select">Add another team</a>';

        }else{
            var TeamsHTML = '<a class="team-select">Select your team</a>';
        }

        $('#user-teams').html(TeamsHTML);

        $$('.swipeout').on('deleted', function (e) {
            DeleteUserTeam($(this).attr('data-rel'));
        });

        $$('.swipeout-view').on('click', function () {
            mainView.router.loadPage({
                url: 'view-team.html',
                query: {
                    TeamID: $(this).attr('id')
                    }
                });
            myApp.closeModal('.modal-overlay');
            myApp.closeModal('.popup');
        });

        $('body').removeClass('unauthorised');
        $('body').addClass('signed-in');
        $('.user-details').removeClass('sign-in');
        /*LogOut Button*/
            if(!$$('.sign-out').length){
                Notifications();
                if(storedData.User.Role == 'administrator'){
                    $$('.menu .teams').append('<li><a href="add-team.html" data-transition="slide" class="sub-menu" data-role="button">Add a Team</a></li>');
                    $$('.menu .tournament').append('<li><a href="add-tournament.html" data-transition="slide" class="sub-menu" data-role="button">Create a Tournament</a></li>');
                }
                if(storedData.Games){
                    var Active = '';
                    if(storedData.Games.Active){
                        Active = '<div class="badge bg-red">' + storedData.Games.Active.length + '</div>';
                    }
                    $$('.menu .matches').prepend('<li><a href="view-matches.html" data-transition="slide" class="sub-menu" data-role="button">View Matches' + Active + '</a></li>');
                }
                $$('.menu .content-block').append('<a href="#" class="sign-out" data-transition="slide" data-role="button"><i class="fa fa-sign-out" aria-hidden="true"></i> Logout</a>');
            }
            SignOut();
        /*LogOut Button*/
        
        return true;
    }else{
        $('body').removeClass('signed-in').addClass('unauthorised');
        $('.user-details').addClass('sign-in');
        return false;
    }
}

function RegisterUser(){
    $('#UserRegister button').on('click',function(){
        var formname = 'UserRegister';
        if(validation(formname)){   
            /* Sign In */
            preloader('', 'body');
            $.ajax({
                type: "POST",
                dataType: 'jsonp',
                url : "http://groot.dedicated.co.za/cric-it/api",
                data : 'wrapper=user&type=RegisterUser&SendMail=true&FirstName=' + $('input[name="FirstName"]').val() + '&LastName=' + $('input[name="LastName"]').val() + '&Email=' + $('input[name="EmailRegister"]').val() + '&Password=' + $('input[name="PasswordRegister"]').val() + '&os=' + myApp.device.os + '&deviceID=' + localStorage.getItem('uuid') + '&_token=t8Y]^RhRe,xp@Xfk76%$',
                success : function(data){
                    if(data.status == 'false'){
                        mainView.router.loadPage('forgotten.html');
                        myApp.addNotification({
                            title: 'Forgotten your password?',
                            message: data.result,
                            hold: 5000
                        });
                    }else{
                        /* Sign In */
                        var storedData = myApp.formStoreData('UserDetails', {
                            'User': data.result.User
                        });
                        mainView.router.loadPage('home.html');
                    }
                    preloaderRemove();
                },
                error : function(data){
                    preloaderRemove();
                    myApp.addNotification({
                        title: 'Error',
                        message: 'Oops! Something went wrong, please try again later.',
                        hold: 5000
                    });
                }
            },"json");
            return false;
        }    
        return false;
    });
}

function ForgottenUser(){
    $('#UserForgotten .btn-primary.round').on('click',function(){
        var formname = 'UserForgotten';
        if(validation(formname)){   
            /* Sign In */
            preloader('', 'body');
            $.ajax({
                type: "POST",
                dataType: 'jsonp',
                url : "http://groot.dedicated.co.za/cric-it/api",
                data : 'wrapper=user&type=ForgottenUser&Email=' + $('input[name="EmailForgotten"]').val() + '&SendMail=true&_token=t8Y]^RhRe,xp@Xfk76%$',
                success : function(data){
                    if(data.status == 'false'){
                        mainView.router.loadPage('register.html');
                        myApp.addNotification({
                            title: 'Please register',
                            message: data.result,
                            hold: 5000
                        });
                    }else{
                        mainView.router.loadPage('login.html');
                        myApp.addNotification({
                            title: 'Sign In',
                            message: 'Please Sign In with your New Password sent to ' + data.result,
                            hold: 5000
                        });
                    }
                    preloaderRemove();
                },
                error : function(data){
                    preloaderRemove();
                    myApp.addNotification({
                        title: 'Error',
                        message: 'Oops! Something went wrong, please try again later.',
                        hold: 5000
                    });
                }
            },"json");
            return false;
        }    
        return false;
    });
}

function CheckUser(){
    $('#UserSignIn button').on('click',function(){
        var formname = 'UserSignIn';
        if(validation(formname)){   
            /* Sign In */
            preloader('', 'body');
            $.ajax({
                type: "POST",
                dataType: 'jsonp',
                url : "http://groot.dedicated.co.za/cric-it/api",
                data : 'wrapper=user&type=SignInUser&Email=' + $('input[name="Email"]').val() + '&Password=' + $('input[name="Password"]').val() + '&os=' + myApp.device.os + '&deviceID=' + localStorage.getItem('uuid') + '&_token=t8Y]^RhRe,xp@Xfk76%$',
                success : function(data){
                    if(data.status == 'false'){
                        mainView.router.loadPage('register.html');
                        myApp.addNotification({
                            title: 'Please register',
                            message: data.result,
                            hold: 5000
                        });
                    }else if(data.status == 'failed'){
                        mainView.router.loadPage('forgotten.html');
                        myApp.addNotification({
                            title: 'Password Incorrect',
                            message: data.result,
                            hold: 5000
                        });
                    }else{
                        /* Sign In */
                        var storedData = myApp.formStoreData('UserDetails', {
                            'User': data.result.User,
                            'Roles': data.result.Roles
                        });
                        mainView.router.loadPage('home.html');
                    }
                    preloaderRemove();
                },
                error : function(data){
                    preloaderRemove();
                    myApp.addNotification({
                        title: 'Error',
                        message: 'Oops. Something went wrong!',
                        hold: 5000
                    });
                }
            },"json");
            return false;

            
        }    
        return false;
    });
}

function onProfileImageSuccess(imageURI) {
    preloader('', 'body');
    var storedData = myApp.formGetData('UserDetails');
    var options = new FileUploadOptions();
    options.fileKey="file";
    options.fileName=imageURI.substr(imageURI.lastIndexOf('/')+1);
    options.mimeType="image/jpeg";
    options.httpMethod = 'POST';
    var params = new Object();
    params.UserID = storedData.User.id;
    params.wrapper = 'user';
    params.type = 'UserImageUpdate';

    options.params = params;
    options.chunkedMode = false;
    options.headers = {
        Connection: "close"
    }
    var ft = new FileTransfer();
    ft.upload(imageURI, "http://groot.dedicated.co.za/cric-it/api/index.php", UploadWin, UploadFail, options);
    var image = document.getElementById('profile-img');
    image.src = imageURI;
    preloaderRemove();
    return false;
}

function onProfileImageFail(message) {
    console.log('Failed because: ' + message);
}

function SignOut(){
    $('.sign-out').on('click',function(){
        SignOutAction();
    })
}

function SignOutAction(){
    var formname = 'UserDetails';
    myApp.formDeleteData(formname);
    myApp.closePanel();
    $('body').removeClass('signed-in');
    $('body').addClass('unauthorised');
    $('.user-details').addClass('sign-in');
    alert('You have been signed out');
    mainView.router.loadPage('login.html');
}
////////////// User ///////////////////////