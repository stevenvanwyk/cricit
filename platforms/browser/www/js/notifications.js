////////////// Notifications ///////////////////////
function Notifications(unread){
}
function NotificationsGet(){
}
function NotificationsOld(unread){
    var storedData = myApp.formGetData('UserDetail');
    if(storedData) {
        $.ajax({
            type: "POST",
            dataType: 'jsonp',
            url : "http://groot.dedicated.co.za/bdeattorneys/api",
            data : 'wrapper=notificationsget&Email=' + storedData.Email + '&unread=1&_token=t8Y]^RhRe,xp@Xfk',
            success : function(data){
                
                $('.notification-btn-menu').remove();
                var MenuButton = '<a href="notifications.html" data-transition="slide" data-role="button" class="notification-btn-menu">Notifications';
                if(data.length > 0){
                    MenuButton += '<div class="notification-count">' + data.length + '</div>';
                }
                MenuButton += '</a>';
                $$('.menu .content-block').prepend(MenuButton);

                if(data.length > 0){
                    $('.notification-btn').remove();
                    $$('.navbar .navbar-inner .left').append('<a href="notifications.html" data-transition="slide" data-role="button" class="notification-btn"><div class="notification-count">' + data.length + '</div></a>');
                }
            },
            error : function(data){
            }
        },"json");
    }
}

function NotificationsGetOld(){
    var storedData = myApp.formGetData('UserDetail');
    if(storedData) {
        $.ajax({
            type: "POST",
            dataType: 'jsonp',
            url : "http://groot.dedicated.co.za/bdeattorneys/api",
            data : 'wrapper=notificationsget&Email=' + storedData.Email + '&unread=0&_token=t8Y]^RhRe,xp@Xfk',
            success : function(data){
                var result = '<div class="list-block"><ul>';
                $.each(data,function(k,v){
                    var readit = '';
                    if(v.notification_read == '1'){
                      var readit = 'readit';  
                    }
                    result += '<li class="swipeout">';
                        result += '<div class="item-content swipeout-content ' + readit + '">';
                            result += '<div class="item-inner">';
                                result += '<div class="item-title"><strong>' + v.title + '</strong><br/>' + v.message + '</div>';
                            result += '</div>';
                        result += '</div>';
                        result += '<div class="swipeout-actions-right">';
                            if(v.notification_read == '0'){
                                result += '<a href="#" class="read" data-rel="' + v.id + '" data-type="read">Read</a>';
                            }
                            result += '<a href="#" class="delete" data-rel="' + v.id + '" data-type="delete">Delete</a>';
                        result += '</div>';
                    result += '</li>';
                });
                result += '</ul></div>';
                $$('.page-content .content-block').append(result);
                ReadDelete();
            },
            error : function(data){
            }
        },"json");
    }
}

function ReadDelete(){
    $('.read,.delete').on('click',function(){
        $.ajax({
            type: "POST",
            dataType: 'jsonp',
            url : "http://groot.dedicated.co.za/bdeattorneys/api",
            data : 'wrapper=notificationsreaddelete&id=' + $(this).attr('data-rel') + '&type=' + $(this).attr('data-type') + '&_token=t8Y]^RhRe,xp@Xfk',
            success : function(data){
                mainView.router.refreshPage();
            },
            error : function(data){
                mainView.router.refreshPage();
            }
        },"json");
        
    });
}
////////////// Notifications ///////////////////////