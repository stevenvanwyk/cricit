var myApp = new Framework7();
var CoOrdsX = '-29.837804';
var CoOrdsY = '30.9129848';

var $$ = Dom7;

var mainView = myApp.addView('.view-main', {
    dynamicNavbar: true,
    cache: false,
    pushState: true
});

$$(document).on('deviceready', function() {
    
    var push = PushNotification.init({
        android: {senderID: "111468586615"},
        browser: {pushServiceURL: 'http://push.api.phonegap.com/v1/push'},
        ios: {alert: "true",badge: "true",sound: "true"},
        windows: {}
    });

    push.on('registration', function(data) {
        console.log('registration event: ' + data.registrationId);
        var oldRegId = localStorage.getItem('uuid');
        if (oldRegId !== data.registrationId) {localStorage.setItem('uuid', data.registrationId);}
        var parentElement = document.getElementById('registration');
        var listeningElement = parentElement.querySelector('.waiting');
        var receivedElement = parentElement.querySelector('.received');
        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');
    });

    push.on('error', function(e) {console.log("push error = " + e.message);});

    push.on('notification', function(data) {
        console.log('notification event');
        navigator.notification.alert(data.message,null, data.title,'Ok');
        alert( JSON.stringify(data) );
    });

    navigator.geolocation.getCurrentPosition(onGeoSuccess, onGeoError, { enableHighAccuracy:true });

    $('.content-block.extend-it').each(function(){ContentBlockExtendIt($(this));});

    $('.secondary.extend-it').each(function(){ExtendIt($(this));});

    $('.popup-settings .profile-header .upload').on('click',function(){
        navigator.camera.getPicture(onProfileImageSuccess, onProfileImageFail, { 
            quality: 80,
            destinationType: Camera.DestinationType.FILE_URI,
            sourceType: Camera.PictureSourceType.SAVEDPHOTOALBUM, 
            encodingType: Camera.EncodingType.JPEG,
            mediaType: Camera.MediaType.PICTURE,
            allowEdit: true,
            correctOrientation: true,
            targetWidth: 100,
            targetHeight: 100
        });
    });
 
    initAd();
        
    if(UserDetail()){mainView.router.loadPage('home.html');return false;}

    console.log("Device is ready!");
});

$$(document).on('pageInit', function (e) {
    
    $('.navbar.header .fa-cogs').on('click',function(){
        UserDetail();
        FindFormats('user');
        myApp.popup('.popup-settings', true, true);
    });

    var page = e.detail.page;
    if (page.name === 'index-page' || page.name === 'login' || page.name === 'register' || page.name === 'forgotten') {
        preloaderRemove();
        if(UserDetail()){
            mainView.router.loadPage('home.html');
            return false;
        }else{
            if (page.name === 'login') {
                ShowHidePasswordFields();
                CheckUser();
            }
            if (page.name === 'register') {
                ShowHidePasswordFields();
                RegisterUser();
            }
            if (page.name === 'forgotten') {
                ForgottenUser();
            }
        }
    }

    if (page.name === 'home') {
        myApp.closePanel();
        preloaderRemove();
        UserDetail();
        WhatsAppShare();
        Notifications('0');
        var storedData = myApp.formGetData('UserDetails');
        if (!storedData.Roles) {
            var popupHTML = '<div class="popup popup-usertype">'+
                '<div class="content-block">'+
                    '<h1>What are you?</h1>'+
                    '<a href="#" data-type="player"><i class="fa fa-gamepad" aria-hidden="true"></i>&nbsp;I am a Player</a>'+
                    '<a href="#" data-type="supporter"><i class="fa fa-users" aria-hidden="true"></i>&nbsp;I am a Supporter</a>'+
                    '<a href="#" data-type="administrator"><i class="fa fa-cog" aria-hidden="true"></i>&nbsp;I am an Administrator</a>'+
                '</div>'+
            '</div>'
            myApp.popup(popupHTML, true, true);
            $('.popup .content-block a').on('click',function(){
                UserTypeUpdate($(this).attr('data-type'),storedData.User.id);
            });
        }
    }

    if (page.name === 'club-cricket' || page.name === 'school-cricket' || page.name === 'social-cricket') {
        var Team1 = false;
        var Team2 = false;
        if(page.query.Team1){
            Team1 = page.query.Team1;
            $('.smart-select-close').attr('href','add-match.html');
        }
        if(page.query.Team2){
            Team2 = page.query.Team2;
            $('.smart-select-close').attr('href','add-match.html');
        }
        myApp.closePanel();
        UserDetail();
        Teams(page.name, page.query.Which, page.query.UpdateField, Team1, Team2);
        Notifications('0');
    }

    if (page.name === 'add-team') {
        myApp.closePanel();
        UserDetail();
        AddTeam();
        Notifications('0');
    }

    if (page.name === 'add-match') {
        var Team1 = false;
        var Team2 = false;
        if(page.query.Team1){
            Team1 = page.query.Team1;
            $('#Team1').val(Team1.id);
            $('.Team1-Heading').html(Team1.name);
            $('.Team1').html('Change Team One');
            $('.vs').show();
        }
        if(page.query.Team2){
            Team2 = page.query.Team2;
            $('#Team2').val(Team2.id);
            $('.Team2-Heading').html(Team2.name);
            $('.Team2').html('Change Team Two');
        }
        if(page.query.Team1 && page.query.Team1){
            $('.Match-Settings').show();
        }
        myApp.closePanel();
        UserDetail();
        FindFormats('match', Team1.id, Team2.id);
        AddMatch();
        Notifications('0');
    }

    if (page.name === 'view-match') {
        myApp.closePanel();
        UserDetail();
        GetMatch(page.query.MatchID);
        Notifications('0');
    }

    if (page.name === 'view-matches') {
        myApp.closePanel();
        UserDetail();
        GetMatches();
        Notifications('0');
    }

    if (page.name === 'view-tournaments') {
        myApp.closePanel();
        UserDetail();
        GetTournaments();
        Notifications('0');
    }

    if (page.name === 'view-tournament') {
        $('[data-page="view-tournament"] .teamdetails .logo .upload').on('click',function(){
            navigator.camera.getPicture(onTournamentImageSuccess, onTournamentImageFail, { 
                quality: 80,
                destinationType: Camera.DestinationType.FILE_URI,
                sourceType: Camera.PictureSourceType.SAVEDPHOTOALBUM, 
                encodingType: Camera.EncodingType.JPEG,
                mediaType: Camera.MediaType.PICTURE,
                allowEdit: true,
                correctOrientation: true,
                targetWidth: 100,
                targetHeight: 100
            });
        });

        myApp.closePanel();
        UserDetail();
        ViewTournament(page.query.TournamentID);
        var mySwiper = myApp.swiper('.swiper-container', {
            pagination:'.swiper-pagination'
          });
        Notifications('0');
    }

    if (page.name === 'view-teams') {
        myApp.closePanel();
        UserDetail();
        GetTeams();
        Notifications('0');
    }

    if (page.name === 'add-tournament') {
        $('.Match-Settings').show();
        myApp.closePanel();
        UserDetail();
        AddTournament();
        DateTimePicker('#picker-startdate');
        DateTimePicker('#picker-enddate');
        Notifications('0');
    }

    if (page.name === 'view-team') {
        $('.teamdetails .logo .upload').on('click',function(){
            navigator.camera.getPicture(onTeamImageSuccess, onTeamImageFail, { 
                quality: 80,
                destinationType: Camera.DestinationType.FILE_URI,
                sourceType: Camera.PictureSourceType.SAVEDPHOTOALBUM, 
                encodingType: Camera.EncodingType.JPEG,
                mediaType: Camera.MediaType.PICTURE,
                allowEdit: true,
                correctOrientation: true,
                targetWidth: 100,
                targetHeight: 100
            });
        });

        myApp.closePanel();
        UserDetail();
        ViewTeam(page.query.TeamID);
        var mySwiper = myApp.swiper('.swiper-container', {
            pagination:'.swiper-pagination'
          });
        Notifications('0');
    }

    if (page.name === 'find-an-attorney') {
        myApp.closePanel();
        UserDetail();
        FindStaffList();
        FindAttorney();
        Notifications('0');
    }

    if (page.name === 'notifications') {
        myApp.closePanel();
        UserDetail();
        NotificationsGet();
        Notifications('0');
    }

    if (page.name === 'ask-a-legal-question') {
        myApp.closePanel();
        AskQuestion();
        Notifications('0');
    }

    if (page.name === 'conveyancing-calculator') {
        myApp.closePanel();
        Notifications('0');
    }

    if (page.name === 'conveyancing-calc-repayment') {
        myApp.closePanel();
        Calculate();
        Notifications('0');
    }

    if (page.name === 'conveyancing-calc-transfer') {
        myApp.closePanel();
        Calculate();
        Notifications('0');
    }

    if (page.name === 'conveyancing-calc-bond') {
        myApp.closePanel();
        Calculate();
        Notifications('0');
    }

    if (page.name === 'emergency') {
        myApp.closePanel();
        Emergency();
        Notifications('0');        
    }

    if (page.name === 'contact-us') {
        myApp.closePanel();
        WhatsAppShare();
        Notifications('0');        
    }

    if(page.name === 'find-our-offices'){
        
        myApp.closePanel();
        OpenInMapsLinksPopulate();

        var map;
        map = new GMaps({
            el: '#map',
            lat: CoOrdsX,
            lng: CoOrdsY,
            zoomControl : true,
            zoomControlOpt: {
                style : 'SMALL',
                position: 'TOP_LEFT'
            },
            panControl : false,
            streetViewControl : false,
            mapTypeControl: false,
            overviewMapControl: false
        });

        map.addMarker({
          lat: CoOrdsX,
          lng: CoOrdsY,
          title: 'BDE Attorneys',
        });

        var styles = [
            {
              stylers: [
                { hue: "#364353" },
                { saturation: 0 }
              ]
            }, {
                featureType: "road",
                elementType: "geometry",
                stylers: [
                    { lightness: 100 },
                    { visibility: "simplified" }
              ]
            }, {
                featureType: "road",
                elementType: "labels",
                stylers: [
                    { visibility: "on" }
              ]
            }
        ];

        map.addStyle({
            styledMapName:"Styled Map",
            styles: styles,
            mapTypeId: "map_style"  
        });
        
        map.setStyle("map_style");

        $('#map').each(function(){
            ExtendIt($(this));
        });        
        Notifications('0');
    }

    $('.content-block.extend-it').each(function(){
        ContentBlockExtendIt($(this));
    });
    $('.secondary.extend-it').each(function(){
        ExtendIt($(this));
    });


})

$$(document).on('pageInit', '.page[data-page="about"]', function (e) {

})