////////////// Default ///////////////////////
function ShowHidePasswordFields(){
    $('#UserRegister .showpassword').on('click',function(){
        var _This = $(this);
        if(_This.hasClass('fa-eye')){
            _This.addClass('fa-eye-slash').removeClass('fa-eye');
            $('#PasswordRegister').showPassword();
        }else if(_This.hasClass('fa-eye-slash')){
            _This.addClass('fa-eye').removeClass('fa-eye-slash');
            $('#PasswordRegister').hidePassword();
        }
    });
    $('#UserSignIn .showpassword').on('click',function(){
        var _This = $(this);
        if(_This.hasClass('fa-eye')){
            _This.addClass('fa-eye-slash').removeClass('fa-eye');
            $('#Password').showPassword();
        }else if(_This.hasClass('fa-eye-slash')){
            _This.addClass('fa-eye').removeClass('fa-eye-slash');
            $('#Password').hidePassword();
        }
    });
}

function UploadWin(r) {
    console.log("Code = " + r.responseCode);
    console.log("Response = " + r.response);
    console.log("Sent = " + r.bytesSent);
}

function UploadFail(error) {
    console.log("upload error source " + error.source);
    console.log("upload error target " + error.target);
}

function socialSharing() {
  window.plugins.socialsharing.available(function(isAvailable) {
    if (isAvailable) {
      window.plugins.socialsharing.share('BDE Attorneys ' + $title+':\n'+$results.replace(/(<([^>]+)>)/ig,"").replace(/_newline_/g,'\n'), null, null);
    }
  });
}

function FindFields(formname){
    var fields = "";
    $('#' + formname + ' input,#' + formname + ' textarea,#' + formname + ' select').each(function(k){
        if($(this).attr('name') != '_token'){
            fields += '&' + $(this).attr('name') + '=' + $(this).val();
        }
    });
    return fields;
}

function validation( formname,  data ){
    $validated = 0;
    $('.alert-danger').remove();
    $formname = formname;
    /* validation */
    $('#' + formname + ' input,#' + formname + ' textarea,#' + formname + ' select').filter('[required]:visible').each(function(k, requiredField){
        // $(this).css('border', '1px #ccc solid');
        var requiredcheck = $(this).attr('required');
        if (typeof requiredcheck !== typeof undefined && requiredcheck !== false) { 
            $error_msg = $(this).attr('data-required');
            if($(this).attr('type') == 'email'){
                var pattern = new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
                if(pattern.test($(this).val())){
                    if($formname){
                        if($formname == 'registration'){
                            $token = $('input[name="_token"]').val();
                            if(checkuser($(this).val(), $token)){
                                $value = '';
                                $validated++;
                            }
                        }
                    }
                    $value = 'selected';
                }else{
                    $value = '';
                    $validated++;
                }
            }
            else if($(this).attr('type') == 'radio'){
                if($(this).is(':checked')){
                    $value = 'selected';
                }else{
                    $value = '';
                    $validated++;
                }
            }
            else if($(this).attr('type') == 'password'){
                if($(this).val().length < 5){
                    $error_msg = 'Please enter at least 5 characters';
                    $value = '';
                    $validated++;
                }else if($(this).val().length >= 5){
                    $value = 'true';
                }

                if($(this).attr('data-type') == 'login'){
                    $error_msg = $(this).attr('data-required');
                }

                if($(this).attr('data-copy')){
                    if($(this).val() == $('#' + $(this).attr('data-copy')).val()){
                        $value = 'true';
                    }else{
                        $error_msg = 'This is not the same as your password.';
                        $value = '';
                        $validated++;
                    }
                }   
            }else if($(this).attr('type') == 'select'){
                if($(this).val()){
                    $value = 'selected';
                }else{
                    $value = '';
                    $validated++;
                }
            }else{
                $value = $(this).val().replace(/ /g,'');
            }

            if(!$value){
                $(this).closest('button').css('background-color','#fff').css('color','#fff');
                $(this).css('border', '2px solid #b71318');
                $(this).addClass('warning-input').attr('placeholder','*' + $error_msg);
                $validated++;
            }else{}
        }
    });

        /* select */
        $('#' + formname + ' select').each(function(){
            // $(this).css('border', '1px #ccc solid');
            if($(this).attr('required')){   
                $error_msg = $(this).attr('data-required');
                $value = $(this).val().replace(/ /g,'');
                if(!$value){
                    $(this).css('border', '2px solid #b71318');
                    $(this).addClass('warning-input');
                    $validated++;
                }else{}
            }

        });
        /* select */    
    /* validation */
    if ($validated > 0) {
        return false;
    } else {
        return true;
    }
}

function preloader(msg, which){
    preloaderRemove();
    $(which).append($('<div class="overlay_preloader"><img src="img/preloader.svg"/><p>' + msg + '</p></div>').show().css({'opacity':0}).animate({'opacity':1}));
}

function preloaderRemove(){
    $('.overlay_preloader').fadeOut(800, function(){
        $('.overlay_preloader').remove();
    });
}

function DateTimePicker(Which = false){
    var today = new Date();
    var pickerInline = myApp.picker({
        input: Which,
        toolbar: true,
        rotateEffect: false,
        value: [today.getMonth(), today.getDate(), today.getFullYear(), today.getHours()+1, (today.getMinutes() < 59 ? '00' : today.getMinutes())],
        onChange: function (picker, values, displayValues) {
            var daysInMonth = new Date(picker.value[2], picker.value[0]*1 + 1, 0).getDate();
            if (values[1] > daysInMonth) {
                picker.cols[1].setValue(daysInMonth);
            }
        },
        formatValue: function (p, values, displayValues) {
            var MonthValues = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
            return MonthValues[values[0]] + ' ' + values[1] + ', ' + values[2] + ' ' + values[3] + ':' + values[4];
        },
        cols: [
            // Months
            {
                values: ('0 1 2 3 4 5 6 7 8 9 10 11').split(' '),
                displayValues: ('January February March April May June July August September October November December').split(' '),
                textAlign: 'left'
            },
            // Days
            {
                values: [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31],
            },
            // Years
            {
                values: (function () {
                    var arr = [];
                    for (var i = today.getFullYear(); i <= today.getFullYear() + 1; i++) { arr.push(i); }
                    return arr;
                })(),
            },
            // Space divider
            {
                divider: true,
                content: '  '
            },
            // Hours
            {
                values: (function () {
                    var arr = [];
                    for (var i = 8; i <= 17; i++) { arr.push(i); }
                    return arr;
                })(),
            },
            // Divider
            {
                divider: true,
                content: ':'
            },
            // Minutes
            {
                values: (function () {
                    var arr = [];
                    for (var i = 0; i <= 59; i++) { 
                        if(i == '0' || i == '15' || i == '30' || i == '45'){
                            arr.push(i < 10 ? '0' + i : i); 
                        }
                    }
                    return arr;
                })(),
            }
        ]
    });
}

function ContentBlockExtendIt(This){
    var _This = This;
    var Top = eval(_This.position().top)-5;
    var Height = eval($(window).height());
    var ToolbarHeight = eval($('.navbar.header').height());
    ToolbarHeight = 0;
    var NewHeight = Height-(ToolbarHeight+Top);
    _This.height(NewHeight);
}

function ExtendIt(This){
    var _This = This;
    var Top = eval(_This.position().top);
    var Height = eval($(window).height());
    var ToolbarHeight = eval($('.toolbar').height());
    ToolbarHeight = 0;
    var NewHeight = Height-(ToolbarHeight+Top);
    _This.height(NewHeight);
}

var slug = function(str) {
    var $slug = '';
    var trimmed = $.trim(str);
    $slug = trimmed.replace(/[^a-z0-9-]/gi, '-').
    replace(/-+/g, '-').
    replace(/^-|-$/g, '');
    return $slug.toLowerCase();
}

function WhatsAppShare(){
    $('.whatsappshare').on('click',function(){
        if(myApp.device.os == 'ios'){
            window.plugins.socialsharing.share('Check out the BDE app on the App Store', 'BDE',null,'http://bit.ly/2vti9u9');
        }else if(myApp.device.os == 'android'){
            window.plugins.socialsharing.share('Check out the BDE app on the Play Store', 'BDE',null,'http://bit.ly/2uzvvsx');
        }
    });
}

////////////// Default ///////////////////////